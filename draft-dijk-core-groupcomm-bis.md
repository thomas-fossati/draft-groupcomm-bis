---
title: Group Communication for the Constrained Application Protocol (CoAP)
abbrev: Group Communication for CoAP
docname: draft-dijk-core-groupcomm-bis-02


# stand_alone: true

ipr: trust200902
area: Internet
wg: CoRE Working Group
kw: Internet-Draft
cat: std
obsoletes: 7390
updates: 7252, 7641, 7959

coding: us-ascii
pi:    # can use array (if all yes) or hash here

  toc: yes
  sortrefs:   # defaults to yes
  symrefs: yes

author:
      -    
        ins: E. Dijk
        name: Esko Dijk
        org:    IoTconsultancy.nl
        street: \________________\
        city: Utrecht
        country: The Netherlands
        email: esko.dijk@iotconsultancy.nl
      -
        ins: C. Wang
        name: Chonggang Wang
        org: InterDigital
        street: 1001 E Hector St, Suite 300
        city: Conshohocken
        code: PA 19428
        country: United States
        email: Chonggang.Wang@InterDigital.com
      -
        ins: M. Tiloca
        name: Marco Tiloca
        org: RISE AB
        street: Isafjordsgatan 22
        city: Kista
        code: SE-16440 Stockholm
        country: Sweden
        email: marco.tiloca@ri.se

normative:
  RFC1122:
  RFC2119:
  RFC3376:
  RFC3810:
  RFC4443:
  RFC4944:
  RFC6690:
  RFC7049:
  RFC7252:
  RFC7641:
  RFC7959:
  RFC8075:
  RFC8152:
  RFC8174:
  RFC8613:
  I-D.ietf-core-oscore-groupcomm:

informative:
  I-D.ietf-ace-oauth-authz:
  I-D.ietf-ace-key-groupcomm-oscore:
  I-D.tiloca-core-oscore-discovery:
  I-D.ietf-core-resource-directory:
  I-D.ietf-core-coap-pubsub:
  I-D.ietf-core-multipart-ct:
  RFC6092:
  RFC6550:
  RFC6636:
  RFC7258:
  RFC7346:
  RFC7390:
  RFC7731:
  RFC7967:
  RFC8323:
  Californium:
    author:
      org: Eclipse Foundation
    title: Eclipse Californium
    date: 2019-03
    target: https://github.com/eclipse/californium/tree/2.0.x/californium-core/src/main/java/org/eclipse/californium/core
  Go-OCF:
    author:
      org: Open Connectivity Foundation (OCF)
    title: Implementation of CoAP Server & Client in Go
    date: 2019-03
    target: https://github.com/go-ocf/go-coap

--- abstract

This document specifies the use of the Constrained Application Protocol (CoAP) for group communication, using UDP/IP multicast as the underlying data transport.
Both unsecured and secured CoAP group communication are specified. Security is achieved by use of the Group Object Security for Constrained RESTful Environments (Group OSCORE) protocol. The target application area of this specification is any group communication use cases that involve resource-constrained networked nodes. The most common of such use cases are listed in this document.


--- middle

# Introduction # {#chap-intro}
This document specifies group communication using the Constrained Application Protocol (CoAP) {{RFC7252}} together with UDP/IP multicast. CoAP is a RESTful communication protocol that is used in resource-constrained nodes, and in resource-constrained networks where packet sizes should be small. This area of use is summarized as Constrained RESTful Environments (CoRE).

One-to-many group communication can be achieved in CoAP, by a client using UDP/IP multicast data transport to send multicast CoAP request messages. In response, each server in the addressed group sends a response message back to the client over UDP/IP unicast. Notable CoAP implementations supporting group communication include the framework "Eclipse Californium" 2.0.x {{Californium}} from the Eclipse Foundation and the "Implementation of CoAP Server & Client in Go" {{Go-OCF}} from the Open Connectivity Foundation (OCF).

Both unsecured and secured CoAP group communication over UDP/IP multicast are specified in this document. Security is achieved by using Group Object Security for Constrained RESTful Environments (Group OSCORE) {{I-D.ietf-core-oscore-groupcomm}}, which in turn builds on Object Security for Constrained Restful Environments (OSCORE) {{RFC8613}}. This method provides end-to-end application-layer security protection of CoAP messages, by using CBOR Object Signing and Encryption (COSE) {{RFC7049}}{{RFC8152}}.

All sections in the body of this document are normative, while appendices are informative. For additional background about use cases for CoAP group communication in resource-constrained devices and networks, see {{appendix-usecases}}.

## Scope ## {#scope}
For group communication, only solutions that use CoAP over UDP/multicast are in the scope of this document. There are alternative methods to achieve group communication using CoAP, for example Publish-Subscribe {{I-D.ietf-core-coap-pubsub}} which uses a central broker server that CoAP clients access via unicast communication. These methods may be usable for the same or similar use cases as are targeted in this document.

All guidelines in {{RFC7390}} are imported by this document which replaces {{RFC7390}} in this respect. Furthermore, this document adds: how to provide security by using Group OSCORE {{I-D.ietf-core-oscore-groupcomm}} as the default group communication security solution for CoAP; an updated request/response matching rule for multicast CoAP which updates {{RFC7252}}; the multicast use of CoAP Observe which updates {{RFC7641}}; and an extension of the multicast use of CoAP block-wise transfers, which updates {{RFC7959}}.

Security solutions for group communication and configuration other than Group OSCORE are not in scope. General principles for secure group configuration are in scope. The experimental group configuration protocol in Section 2.6.2 of {{RFC7390}} is not in the scope of this document; thus, that remains an experimental protocol. Since application protocols defined on top of CoAP often define their own specific method of group configuration, the experimental protocol of {{RFC7390}} has not been subject to enough experimentation to warrant a change of this status.

## Terminology ## {#terminology}
The key words "MUST", "MUST NOT", "REQUIRED", "SHALL", "SHALL NOT", "SHOULD", "SHOULD NOT", "RECOMMENDED", "NOT RECOMMENDED", "MAY", and "OPTIONAL"
in this document are to be interpreted as described in BCP 14 {{RFC2119}} {{RFC8174}} when, and only when, they appear in all capitals, as shown here.

This specification requires readers to be familiar with CoAP {{RFC7252}} terminology.

# General Group Communication Operation # {#chap-general-groupcomm}
The general operation of group communication, applicable for both unsecured and secured operation, is specified in this section by going through the stack from top to bottom. First, group configuration (e.g. group creation and maintenance which are usually done by an application, user or commissioning entity) is considered in {{sec-groupconf}}. Then the use of CoAP for group communication including support for protocol extensions (block-wise, Observe, PATCH method) follows in {{sec-coap-usage}}. How CoAP group messages are carried over various transport layers is the subject of {{sec-transport}}. Finally, {{sec-other-protocols}} covers the interworking of CoAP group communication with other protocols that may operate in the same network.

## Group Configuration ## {#sec-groupconf}

### Group Definition ### {#sec-groupdef}
A CoAP group is defined as a set of CoAP endpoints,
where each endpoint is configured to receive CoAP multicast requests that are sent to the group's associated IP multicast address and UDP port. An endpoint may be a member of multiple CoAP groups. Group membership(s) of an endpoint may dynamically change over time. A device sending a CoAP request to a group is not necessarily itself a member of this group: it is only a member if it also has a CoAP server endpoint listening to requests for this CoAP group. For secure group communication, a receiver also requires the security context to successfully decrypt and/or verify group messages in order to be a group member.

A CoAP Group URI has the scheme 'coap' and includes in the authority part either an IP multicast address or a group hostname (e.g., Group Fully Qualified Domain Name (FQDN)) that can be resolved to an IP multicast address. A Group URI also contains an optional UDP port number in the authority part. Group URIs follow the regular CoAP URI syntax (Section 6 of {{RFC7252}}).

Besides CoAP groups, that have relevance at the level of networked devices, there can also be application groups defined. An application group has relevance at the application level -- for example an application group could denote all lights in an office room or all sensors in a hallway. There can be a one-to-one or a one-to-many relation between CoAP groups and application groups.

### Group Naming ###
For clients, it is RECOMMENDED to use by default an IP multicast address literal in a configured Group URI, instead of a hostname. This is because DNS infrastructure may not be deployed in many constrained networks. In case a group hostname is used in the Group URI, it can be uniquely mapped to an IP multicast address via DNS resolution - if DNS client functionality is available in the clients and the DNS service is supported in the network. Some examples of hierarchical group FQDN naming (and scoping) for a building control application are shown in Section 2.2 of {{RFC7390}}.

Application groups can be named in many ways, e.g. numbers, IDs, strings or URIs. An application group identifier, if used, is typically included in the path component or query component of a Group URI. Appendix A of {{I-D.ietf-core-resource-directory}} shows registration of application groups into a Resource Directory, along with the CoAP group it maps to.

### Group Creation and Membership ###
To create a CoAP group, a configuring entity defines an IP multicast address (or hostname) for the group and optionally a UDP port number in case it differs from the default CoAP port 5683. Then, it configures one or more devices as listeners to that IP multicast address, with a CoAP server listening on the group's associated UDP port. These devices are the group members. The configuring entity can be, for example, a local application with pre-configuration, a user, a cloud service, or a local commissioning tool. Also, the devices sending requests to the group in the role of CoAP clients need to be configured with the same information, even though they are not necessarily group members. One way to configure a client is to supply it with a CoAP Group URI, possibly together with the required security material in case communication is secured in the group.

For unsecure group communication, any CoAP endpoint may become a group member at any time: there is no (central) configuring entity that needs to provide the security material for the group. This means that group creation and membership cannot be tightly controlled.

The IETF does not define a mandatory, standardized protocol to accomplish these steps. {{RFC7390}} defines an experimental protocol for configuration of group membership for unsecured group communication, based on JSON-formatted configuration resources. This protocol remains experimental. For secure group communication, the part of the process that involves secure distribution of group keys MAY use standardized communication with a Group Manager as defined in {{chap-oscore}}.

The configuration of groups and membership may be performed at different moments in the life-cycle of a device; for example in the factory, at a reseller, on-site during first deployment, or on-site during a system reconfiguration operation.

### Group Maintenance ###
Maintenance of a group includes necessary operations to cope with changes in a system, such as: adding group members, removing group members, reconfiguration of UDP port and/or IP multicast address, reconfiguration of the Group URI, splitting of groups, or merging of groups.

For unsecured group communication (see {{chap-unsecured-groupcomm}}), addition/removal of group members is simply done by configuring these devices to start/stop listening to the group IP multicast address, and to start/stop the CoAP server listening to the group IP multicast address and port.

For secured group communication (see {{chap-oscore}}), the protocol Group OSCORE {{I-D.ietf-core-oscore-groupcomm}} is mandatory to implement. When using Group OSCORE, CoAP endpoints participating in group communication are also members of a corresponding OSCORE group, and thus share a common set of cryptographic material. Additional related maintenance operations are discussed in {{chap-sec-group-maintenance}}.

## CoAP Usage ## {#sec-coap-usage}

### Request/Response Model ### {#sec-request-response}
A CoAP client is an endpoint able to transmit CoAP requests and receive CoAP responses. Since the underlying UDP transport supports multiplexing by means of UDP port number, there can be multiple independent CoAP clients operational on a single host. On each UDP port, an independent CoAP client can be hosted. Each independent CoAP client sends requests that use the associated endpoint's UDP port number as the UDP source port of the request.

All CoAP requests that are sent via IP multicast MUST be Non-confirmable (Section 8.1 of {{RFC7252}}).  The Message ID in an IP multicast CoAP message is used for optional message deduplication by both clients and servers, as detailed in Section 4.5 of {{RFC7252}}.

A server sends back a unicast response to the CoAP group communication request - but it MAY suppress this response if so selected by the server application and if permitted by the rules in this document. The unicast responses received by the CoAP client may be a mixture of success (e.g., 2.05 Content) and failure (e.g., 4.04 Not Found) codes, depending on the individual server processing results.

Response suppression controlled by a server SHOULD be performed in a consistent way, such that if a first request leads to a response that is not suppressed, then a second similar request on the same resource that leads to the same response code is also not suppressed.

The CoAP Option for No Server Response {{RFC7967}} can be used by a client to influence the response suppression on the server side. It is RECOMMENDED for a server to implement this option only on selected resources where it is useful in the application context.

A CoAP client MAY repeat a multicast request using the same Token value and same Message ID value, in order to ensure that enough (or all) group members have been reached with the request. This is useful in case a number of group members did not respond to the initial request. However, in case one or more servers did receive the initial request but the response to that request was lost, this repeat may not help to retrieve the lost response(s) if the server implements the optional Message ID based deduplication.

A CoAP client MAY also repeat a multicast request using the same Token value but a different Message ID, in which case all servers that received the initial request will again process the repeated request. This is useful in case a client needs to collect more, or even all, responses from group members.

The CoAP client can distinguish the origin of multiple server responses by the source IP address of the UDP message containing the CoAP response and/or any other available application-specific source identifiers contained in the CoAP response. While processing a response, the source endpoint of the response is not exactly matched to the destination endpoint of the request, since for a multicast request these will never match. This is specified in Section 8.2 of {{RFC7252}}. In case a single client has sent multiple group requests and concurrent CoAP transactions are ongoing, the responses received by that client are matched to a request using the Token value. Due to UDP level multiplexing, the UDP destination port of the response MUST match to the client endpoint's UDP port value, i.e. to the UDP source port of the client's request.

For multicast CoAP requests, there are additional constraints on the reuse of Token values at the client, compared to the unicast case.  In the unicast case, receiving a response effectively frees up its Token value for reuse, since no more responses to the same request will follow. However, for multicast CoAP, the number of responses is not bound a priori. Therefore, client cannot use the reception of a response as a trigger to "free up" a Token value for reuse. Moreover, reusing a Token value too early could lead to incorrect response/request matching on the client, and would be a protocol error.  Therefore, the time between reuse of Token values used in multicast requests MUST be greater than:

NON_LIFETIME + MAX_LATENCY + MAX_SERVER_RESPONSE_DELAY

where NON_LIFETIME and MAX_LATENCY are defined in Section 4.8 of {{RFC7252}}.  This specification defines MAX_SERVER_RESPONSE_DELAY as in {{RFC7390}}, that is: the expected maximum response delay over all servers that the client can send a multicast request to.  This delay includes the maximum Leisure time period as defined in Section 8.2 of {{RFC7252}}. However, CoAP does not define a time limit for the server response delay.  Using the default CoAP parameters, the Token reuse time MUST be greater than 250 seconds plus MAX_SERVER_RESPONSE_DELAY.  A preferred solution to meet this requirement is to generate a new unique Token for every new multicast request, such that a Token value is never reused.  If a client has to reuse Token values for some reason, and also MAX_SERVER_RESPONSE_DELAY is unknown, then using MAX_SERVER_RESPONSE_DELAY = 250 seconds is a reasonable guideline. The time between Token reuses is in that case set to a value greater than 500 seconds.

Another method to more easily meet the above constraint is to instantiate multiple CoAP clients at multiple UDP ports on the same host. The Token values only have to be unique within the context of a single CoAP client, so using multiple clients can make it easier to meet the constraint.

Since a client sending a multicast request with a Token T will accept multiple responses with the same Token T, there is a risk that the same server sends multiple responses with the same Token T back to the client. For example, this server might not implement the optional CoAP message deduplication based on Message ID, or it might be a malicious/compromised server acting out of specification. To mitigate issues with multiple responses from one server bound to a same multicast request, the client has to ensure that, as long as the the CoAP Token used for a multicast request is retained, at most one response to that request per server is accepted, with the exception of Observe notifications {{RFC7641}} (see {{sec-observe}}).

To this end, upon receiving a response corresponding to a multicast request, the client MUST perform the following actions. First, the client checks whether it previously received a valid response to this request from the same originating server of the just-received response. If the check yields a positive match and the response is not an Observe notification (i.e., it does not include an Observe option), the client SHALL stop processing the response. Upon eventually freeing up the Token value of a multicast request for possible reuse, the client MUST also delete the list of responding servers associated to that request.

<!--
Note that, in case response messages are secured with Group OSCORE (see {{chap-oscore}}), the client has to first successfully decrypt and verify a response, before asserting it to be an Observe notification, i.e. a response including an Inner Observe option with value relevant to the two client and server endpoints (see Section 4.1.3.5 of {{RFC8613}}).
-->

### Port and URI Path Selection ###
A CoAP server that is a member of a group listens for CoAP messages on the group's IP multicast address, usually on the CoAP default UDP port 5683, or another non-default UDP port if configured. Regardless of the method for selecting the port number, the same port number MUST be used across all CoAP servers that are members of a group and across all CoAP clients performing the group requests to that group. The URI Path used in the request is preferably a path that is known to be supported across all group members. However there are valid use cases where a request is known to be successful for a subset of the group and those group members for which the request is unsuccessful either ignore the multicast request or respond with an error status code.

Using different ports with the same IP multicast address is an efficient way to create multiple CoAP groups in constrained devices, in case the device's stack only supports a limited number of IP multicast group memberships. However, it must be taken into account that this incurs additional processing overhead on each CoAP server participating in at least one of these groups: messages to groups that are not of interest to the node are only discarded at the higher transport (UDP) layer instead of directly at the network (IP) layer.

Port 5684 is reserved for DTLS-secured CoAP and MUST NOT be used for any CoAP group communication.

For a CoAP server node that supports resource discovery as defined in Section 2.4 of {{RFC7252}}, the default port 5683 MUST be supported (see Section 7.1 of {{RFC7252}}) for the "All CoAP Nodes" multicast group.

### Proxy Operation ### {#sec-proxy}

CoAP (Section 5.7.2 of {{RFC7252}}) allows a client to request a forward-proxy to process its CoAP request.  For this purpose, the client specifies either the request group URI as a string in the Proxy-URI option or alternatively it uses the Proxy-Scheme option with the group URI constructed from the usual Uri-* options.  This approach works well for unicast requests.  However, there are certain issues and limitations associated with processing the (unicast) responses to a CoAP group communication request made in this manner through a proxy.

A proxy may buffer all the individual (unicast) responses to a CoAP group communication request and then send back only a single (aggregated) response to the client.  The issues with this approach are:

* The proxy does not know how many members there are in the group or how many group members will actually respond. Also, the proxy does not know how long to wait before deciding to send back the aggregated response to the client.

* There is no default format defined in CoAP for aggregation of multiple responses into a single response. Such a format could be defined based on the multipart content-format {{I-D.ietf-core-multipart-ct}} but is not standardized yet currently.

Alternatively, if a proxy does not aggregate responses and follows the CoAP Proxy specification (Section 5.7.2 of {{RFC7252}}), the proxy would forward all the individual (unicast) responses to a CoAP group communication request to the client. There are also issues with this approach:

* The client may be confused as it may not have known that the Proxy-URI contained a group URI target. That is, the client that sent a unicast CoAP request to the proxy may be expecting only one (unicast) response. Instead, it receives multiple (unicast) responses, potentially leading to fault conditions in the application.

* Each individual CoAP response will appear to originate (based on its IP source       address) from the CoAP Proxy, and not from the server that produced the response.  This makes it impossible for the client to identify the server that produced each response, unless the server identity is contained as a part of the response payload.

Due to the above issues, a CoAP Proxy SHOULD NOT support processing an IP multicast CoAP request but rather return a 501 (Not Implemented) response in such case.  The exception case here (i.e., to support it) is when all the following conditions are met:

* The CoAP Proxy MUST be explicitly configured (white-list) to allow proxied IP multicast requests by specific client(s).

*  The proxy SHOULD return individual (unicast) CoAP responses to the client (i.e., not aggregated). If a (future) standardized aggregation format is being used, then aggregated responses may be sent.

* It MUST be known to the person/entity doing the configuration of the proxy, or otherwise verified in some way, that the client configured in the white-list supports receiving multiple responses to a proxied unicast CoAP request.

The operation of HTTP-to-CoAP proxies for multicast CoAP requests is specified in Section 8.4 and 10.1 of {{RFC8075}}. In this case, the "application/http" media type can be used to let the proxy return multiple CoAP responses -- each translated to a HTTP response -- back to the HTTP client. Of course, in this case the HTTP client needs to be aware that it is receiving this format and needs to be able to decode from it the responses of multiple servers. The above restrictions listed for CoAP Proxies still apply to HTTP-to-CoAP proxies: specifically, the IP address of the sender of each CoAP response cannot be determined from the application/http response.

### Congestion Control ### {#sec-congestion}
CoAP group communication requests may result in a multitude of responses from different nodes, potentially causing congestion. Therefore, both the sending of IP multicast requests and the sending of the unicast CoAP responses to these multicast requests should be conservatively controlled.

CoAP {{RFC7252}} reduces IP multicast-specific congestion risks through the following measures:

* A server may choose not to respond to an IP multicast request if there is nothing useful to respond to, e.g., error or empty response (see Section 8.2 of {{RFC7252}}).

* A server should limit the support for IP multicast requests to specific resources where multicast operation is required (Section 11.3 of {{RFC7252}}).

* An IP multicast request MUST be Non-confirmable (Section 8.1 of {{RFC7252}}).

* A response to an IP multicast request SHOULD be Non-confirmable (Section 5.2.3 of {{RFC7252}}).

* A server does not respond immediately to an IP multicast request and should first wait for a time that is randomly picked within a predetermined time interval called the Leisure (Section 8.2 of {{RFC7252}}).

Additional guidelines to reduce congestion risks defined in this document are as follows:

* A server in an LLN should only support group communication GET for resources that are small. This can consist, for example, in having the payload of the response as limited to approximately 5% of the IP Maximum Transmit Unit (MTU) size, so that it fits into a single link-layer frame in case IPv6 over Low-Power Wireless Personal Area Networks (6LoWPAN) (see Section 4 of {{RFC4944}}) is used.

* A server SHOULD minimize the payload size of a response to a multicast GET on "/.well-known/core" by using hierarchy in arranging link descriptions for the response. An example of this is given in Section 5 of {{RFC6690}}.

* A server MAY minimize the payload size of a response to a multicast GET (e.g., on "/.well-known/core") by using CoAP block-wise transfers {{RFC7959}} in case the payload is long, returning only a first block of the CoRE Link Format description.  For this reason, a CoAP client sending an IP multicast CoAP request to "/.well-known/core" SHOULD support block-wise transfers.

* A client SHOULD use CoAP group communication with the smallest possible IP multicast scope that fulfills the application needs. As an example, site-local scope is always preferred over global scope IP multicast if this fulfills the application needs. Similarly, realm-local scope is always preferred over site-local scope if this fulfills the application needs.

### Observing Resources ### {#sec-observe}
The CoAP Observe Option {{RFC7641}} is a protocol extension of CoAP, that allows a CoAP client to retrieve a representation of a resource and automatically keep this representation up-to-date over a longer period of time. The client gets notified when the representation has changed. {{RFC7641}} does not mention whether the Observe Option can be combined with CoAP multicast. This section updates {{RFC7641}} with the use of the Observe Option in a CoAP multicast GET request and defines normative behavior for both client and server.

Multicast Observe is a useful way to start observing a particular resource on all members of a (multicast) group at the same time. Group members that do not have this particular resource or do not allow the GET method on it will either respond with an error status -- 4.04 Not Found or 4.05 Method Not Allowed, respectively -- or will silently suppress the response following the rules of {{sec-request-response}}, depending on server-specific configuration.

A client that sends a multicast GET request with the Observe Option MAY repeat this request using the same Token value and the same Observe Option value, in order to ensure that enough (or all) group members have been reached with the request. This is useful in case a number of group members did not respond to the initial request. The client MAY additionally use the same Message ID in the repeated request to avoid that group members that had already received the initial request would respond again. Note that using the same Message ID in a repeated request will not be helpful in case of loss of a response message, since the server that responded already will consider the repeated request as a duplicate message. On the other hand, if the client uses a different, fresh Message ID in the repeated request, then all the group members that receive this new message will typically respond again, which increases the network load.

A client that sent a multicast GET request with the Observe Option MAY follow up by sending a new unicast CON request with the same Token value and same Observe Option value to a particular server, in order to ensure that the particular server receives the request. This is useful in case a specific group member, that was expected to respond to the initial group request, did not respond to the initial request. The client in this case always uses a Message ID that differs from the initial multicast message.

In the above client behaviors, the Token value is kept identical to the initial request to avoid that a client is included in more than one entry in the list of observers (Section 4.1 of {{RFC7641}}).

Before repeating a request as specified above, the client SHOULD wait for at least the expected round-trip time plus the Leisure time period defined in Section 8.2 of {{RFC7252}}, to give the server time to respond.

A server that receives a legitimate GET request with the Observe Option, for which request processing is successful, SHOULD NOT suppress the response to this request, because the client is obviously interested in the resource representation. A server that adds a client to the list of observers for a resource due to an Observe request MUST NOT suppress the response to this request.

A server SHOULD have a mechanism to verify liveness of its observing clients and the continued interest of these clients in receiving the observe notifications. This can be implemented by sending notifications occassionally using a Confirmable message. See Section 4.5 of {{RFC7641}} for details. This requirement overrides the regular behavior of sending Non-Confirmable notifications in response to a Non-Confirmable request.

For observing a group of servers through a CoAP-to-CoAP proxy or HTTP-CoAP proxy, the limitations stated in {{sec-proxy}} apply.

### Block-Wise Transfer ### {#sec-block-wise}

Section 2.8 of {{RFC7959}} specifies how a client can use block-wise transfer (Block2 Option) in a multicast GET request to limit the size of the initial response of each server. The client has to use unicast for any further requests, separately addressing each different server, in order to retrieve more blocks of the resource from that server, if any. Also, a server (group member) that needs to respond to a multicast request with a particularly large resource can use block-wise transfer (Block2 Option) at its own initiative, to limit the size of the initial response. Again, a client would have to use unicast for any further requests to retrieve more blocks of the resource.

A solution for multicast block-wise transfer using the Block1 Option is not specified in {{RFC7959}} nor in the present document. Such a solution would be useful for multicast PUT/POST/PATCH/iPATCH requests, to efficiently distribute a large request payload as multiple blocks to all members of a CoAP group. Multicast usage of Block1 is non-trivial due to potential message loss (leading to missing blocks or missing confirmations), and potential diverging block size preferences of different members of the multicast group.

## Transport ## {#sec-transport}
In this document only UDP is considered as a transport protocol, both over IPv4 and IPv6. Therefore, {{RFC8323}} (CoAP over TCP, TLS, and WebSockets) is not in scope as a transport for group communication.

### UDP/IPv6 Multicast Transport ### {#sec-udptransport}
CoAP group communication can use UDP over IPv6 as a transport protocol, provided that IPv6 multicast is enabled. IPv6 multicast MAY be supported in a network only for a limited scope. For example, {{sec-rpl}} describes the potential limited support of RPL for multicast, depending on how the protocol is configured.

For a CoAP server node that supports resource discovery as defined in Section 2.4 of {{RFC7252}}, the default port 5683 MUST be supported as per Section 7.1 and 12.8 of {{RFC7252}} for the "All CoAP Nodes" multicast group. An IPv6 CoAP server SHOULD support the "All CoAP Nodes" groups with at least link-local (2), admin-local (4) and site-local (5) scopes. An IPv6 CoAP server on a 6LoWPAN node (see {{sec-6lowpan}}) SHOULD also support the realm-local (3) scope.

Note that a client sending an IPv4 multicast CoAP message to a port that is not supported by the server will not receive an ICMP Port Unreachable error message from that server, because the server does not send it in this case, per Section 3.2.2 of {{RFC1122}}.

### UDP/IPv4 Multicast Transport ###
CoAP group communication can use UDP over IPv4 as a transport protocol, provided that IPv4 multicast is enabled. For a CoAP server node that supports resource discovery as defined in Section 2.4 of {{RFC7252}}, the default port 5683 MUST be supported as per Section 7.1 and 12.8 of {{RFC7252}}, for the "All CoAP Nodes" IPv4 multicast group.

Note that a client sending an IPv6 multicast CoAP message to a port that is not supported by the server will not receive an ICMPv6 Port Unreachable error message from that server, because the server does not send it in this case, per Section 2.4 of {{RFC4443}}.

### 6LoWPAN ### {#sec-6lowpan}
In 6LoWPAN {{RFC4944}} networks, IPv6 packets (up to 1280 bytes) may be fragmented into smaller IEEE 802.15.4 MAC frames (up to 127 bytes), if the packet size requires this. Every 6LoWPAN IPv6 router that receives a multi-fragment packet reassembles the packet and refragments it upon transmission. Since the loss of a single fragment implies the loss of the entire IPv6 packet, the performance in terms of packet loss and throughput of multi-fragment multicast IPv6 packets is typically far worse than the performance of single-fragment IPv6 multicast packets. For this reason, a CoAP request sent over multicast in 6LoWPAN networks SHOULD be sized in such a way that it fits in a single IEEE 802.15.4 MAC frame, if possible.

On 6LoWPAN networks, multicast groups can be defined with realm-local scope {{RFC7346}}. Such a realm-local group is restricted to the local 6LoWPAN network/subnet. In other words, a multicast request to that group does not propagate beyond the 6LoWPAN network segment where the request originated. For example, a multicast discovery request can be sent to the realm-local "All CoAP Nodes" IPv6 multicast group (see {{sec-udptransport}}) in order to discover only CoAP servers on the local 6LoWPAN network.

## Interworking with Other Protocols ## {#sec-other-protocols}

### MLD/MLDv2/IGMP/IGMPv3 ###
<!-- Section 4.2 of {{RFC7390}} has the original content -->

CoAP nodes that are IP hosts (i.e., not IP routers) are generally unaware of the specific IP multicast routing/forwarding protocol
being used in their network.  When such a host needs to join a specific (CoAP) multicast group, it requires a way to signal to IP multicast routers
which IP multicast address(es) it needs to listen to.

The MLDv2 protocol {{RFC3810}} is the standard IPv6 method to achieve this; therefore, this method SHOULD
be used by group members to subscribe to the multicast group IPv6 address, on IPv6 networks that support it. CoAP server nodes then act
in the role of MLD Multicast Address Listener. Constrained IPv6 networks that implement either RPL (see {{sec-rpl}}) or MPL (see {{sec-mpl}}) typically
do not support MLD as they have their own mechanisms defined.

The IGMPv3 protocol {{RFC3376}} is the standard IPv4 method to signal multicast group subscriptions.
This SHOULD be used by group members to subscribe to their multicast group IPv4 address on IPv4 networks.

The guidelines from {{RFC6636}} on the tuning of MLD for mobile and wireless networks may be useful when implementing MLD in LLNs.

### RPL ### {#sec-rpl}
<!-- see Section 4.3 of {{RFC7390}} for original content -->

RPL {{RFC6550}} is an IPv6 based routing protocol suitable for low-power, lossy networks (LLNs). In such a context, CoAP is often used as an application protocol.

If RPL is used in a network for routing and its optional multicast support is disabled, there will be no IP multicast routing available.  Any IPv6 multicast packets in this case will not propagate beyond a single hop (to direct neighbors in the LLN). This implies that any CoAP group communication request will be delivered to link-local nodes only, for any scope value >= 2 used in the IPv6 destination address.

RPL supports (see Section 12 of {{RFC6550}}) advertisement of IP multicast destinations using Destination Advertisement Object (DAO) messages and subsequent routing of multicast IPv6 packets based on this.  It requires the RPL mode of operation to be 3 (Storing mode with multicast support).

In this mode, RPL DAO can be used by a CoAP node that is either an RPL router or RPL Leaf Node, to advertise its IP multicast group membership to parent RPL routers. Then, RPL will route any IP multicast CoAP requests over
multiple hops to those CoAP servers that are group members.

The same DAO mechanism can be used to convey IP multicast group membership information to an edge router (e.g., 6LBR), in case the edge router is also the root of the RPL Destination-Oriented Directed Acyclic Graph (DODAG).  This is useful because the edge router then learns which IP multicast traffic it needs to pass through from the backbone network into the LLN subnet.  In LLNs, such ingress filtering helps to avoid congestion of the resource-constrained network segment, due to IP multicast traffic from the high-speed backbone IP network.

### MPL ### {#sec-mpl}
The Multicast Protocol for Low-Power and Lossy Networks (MPL) {{RFC7731}} can be used for propagation of IPv6 multicast packets throughout a defined network domain, over multiple hops.  MPL is designed to work in LLNs. MPL defines the protocol for a predefined group of MPL Forwarders to collectively distribute IPv6 multicast packets throughout their MPL Domain. An MPL Forwarder may be associated to multiple MPL Domains at the same time. Non-Forwarders will receive IPv6 multicast packets from one or more of their neighboring Forwarders. Therefore, MPL can be used to propagate a CoAP multicast request to all group members.

However, a CoAP multicast request to a group that originated outside of the MPL Domain will not be propagated by MPL - unless an MPL Forwarder is explicitly configured as an ingress point that introduces external multicast packets into the MPL Domain. Such an ingress point could be located on an edge router (e.g., 6LBR). The method to configure which multicast groups are to be propagated into the MPL Domain could be:

* Manual configuration on the ingress MPL Forwarder.

* A protocol to register multicast groups at an ingress MPL Forwarder. This could be a protocol offering features similar to MLDv2.

# Unsecured Group Communication # {#chap-unsecured-groupcomm}

CoAP group communication can operate in CoAP NoSec (No Security) mode, without using application-layer and transport-layer security mechanisms. The NoSec mode uses the "coap" scheme, and is defined in Section 9 of {{RFC7252}}. Before using this mode of operation, the security implications ({{chap-security-considerations-nosec-mode}}) must be well understood.

# Secured Group Communication using Group OSCORE # {#chap-oscore}

The application-layer protocol Object Security for Constrained RESTful Environments (OSCORE) {{RFC8613}} provides end-to-end encryption, integrity and replay protection of CoAP messages exchanged between two CoAP endpoints. These can act both as CoAP Client as well as CoAP Server, and share an OSCORE Security Context used to protect and verify exchanged messages. The use of OSCORE does not affect the URI scheme and OSCORE can therefore be used with any URI scheme defined for CoAP.

OSCORE uses COSE {{RFC8152}} to perform encryption, signing and Message Authentication Code operations, and to efficiently encode the result as a COSE object. In particular, OSCORE takes as input an unprotected CoAP message and transforms it into a protected CoAP message, by using an Authenticated Encryption with Associated Data (AEAD) algorithm.

OSCORE makes it possible to selectively protect different parts of a CoAP message in different ways, so still allowing intermediaries (e.g., CoAP proxies) to perform their intended funtionalities. That is, some message parts are encrypted and integrity protected; other parts are only integrity protected to be accessible to, but not modifiable by, proxies; and some parts are kept as plain content to be both accessible to and modifiable by proxies. Such differences especially concern the CoAP options included in the unprotected message.

Group OSCORE {{I-D.ietf-core-oscore-groupcomm}} builds on OSCORE, and provides end-to-end security of CoAP messages exchanged between members of an OSCORE group, while fulfilling the same security requirements.

In particular, Group OSCORE protects CoAP requests sent over IP multicast by a CoAP client, as well as multiple corresponding CoAP responses sent over IP unicast by different CoAP servers. However, the same keying material can also be used to protect CoAP requests sent over IP unicast to a single CoAP server in the OSCORE group, as well as the corresponding responses.

Group OSCORE uses digital signatures to ensure source authentication of all messages exchanged within the OSCORE group. That is, sender devices sign their outgoing messages by means of their own private key, and embed the signature in the protected CoAP message.

A Group Manager is responsible for one or multiple OSCORE groups. In particular, the Group Manager acts as repository of public keys of group members; manages, renews and provides keying material in the group; and handles the join process of new group members.

As recommended in {{I-D.ietf-core-oscore-groupcomm}}, a CoAP endpoint can join an OSCORE group by using the method described in {{I-D.ietf-ace-key-groupcomm-oscore}} and based on the ACE framework for Authentication and Authorization in constrained environments {{I-D.ietf-ace-oauth-authz}}.

A CoAP endpoint can discover OSCORE groups and retrieve information to join them through their Group Managers by using the method described in {{I-D.tiloca-core-oscore-discovery}} and based on the CoRE Resource Directory {{I-D.ietf-core-resource-directory}}.

If security is required, CoAP group communication as described in this specification MUST use Group OSCORE. In particular, a CoAP group as defined in {{sec-groupdef}} and using secure group communication is associated to an OSCORE group, which includes:

* All members of the CoAP group, i.e. the CoAP endpoints configured (also) as CoAP servers and listening to the group's multicast IP address.

* All further CoAP endpoints configured only as CoAP clients, that send (multicast) CoAP requests to the CoAP group.

## Secure Group Maintenance # {#chap-sec-group-maintenance}

Additional key management operations on the OSCORE group are required, depending also on the security requirements of the application (see {{chap-security-considerations-sec-mode}}). That is:

* Adding new members to a CoAP group or enabling new client-only endpoints to interact with that group require also that each of such members/endpoints join the corresponding OSCORE group. By doing so, they are securely provided with the necessary cryptographic material. In case backward security is needed, this also requires to first renew such material and distribute it to the current members/endpoints, before new ones are added and join the OSCORE group.

* In case forward security is needed, removing members from a CoAP group or stopping client-only endpoints from interacting with that group requires removing such members/endpoints from the corresponding OSCORE group. To this end, new cryptographic material is generated and securely distributed only to the remaining members/endpoints. This ensures that only the members/endpoints intended to remain are able to continue participating in secure group communication, while the evicted ones are not able to.

The key management operations mentioned above are entrusted to the Group Manager responsible for the OSCORE group {{I-D.ietf-core-oscore-groupcomm}}, and it is RECOMMENDED to perform them according to the approach described in {{I-D.ietf-ace-key-groupcomm-oscore}}.

# Security Considerations # {#chap-security-considerations}

This section provides security considerations for CoAP group communication using IP multicast.

## CoAP NoSec Mode ## {#chap-security-considerations-nosec-mode}

CoAP group communication, if not protected, is vulnerable to all the attacks mentioned in Section 11 of {{RFC7252}} for IP multicast.

Thus, for sensitive and mission-critical applications (e.g., health monitoring systems and alarm monitoring systems), it is NOT RECOMMENDED to deploy CoAP group communication in NoSec mode.

Without application-layer security, CoAP group communication SHOULD only be deployed in applications that are non-critical, and that do not involve or may have an impact on sensitive data and personal sphere. These include, e.g., read-only temperature sensors deployed in non-sensitive environments, where the client reads out the values but does not use the data to control actuators or to base an important decision on.

Discovery of devices and resources is a typical use case where NoSec mode is applied, since the devices involved do not have yet configured any mutual security relations at the time the discovery takes place.

## Group OSCORE ## {#chap-security-considerations-sec-mode}

Group OSCORE provides end-to-end application-level security. This has many desirable properties, including maintaining security assurances while forwarding traffic through intermediaries (proxies). Application-level security also tends to more cleanly separate security from the dynamics of group membership (e.g., the problem of distributing security keys across large groups with many members that come and go).

For sensitive and mission-critical applications, CoAP group communication MUST be protected by using Group OSCORE as specified in {{I-D.ietf-core-oscore-groupcomm}}. The same security considerations from Section 8 of {{I-D.ietf-core-oscore-groupcomm}} hold for this specification.

### Group Key Management ### {#chap-security-considerations-sec-mode-key-mgmt}

A key management scheme for secure revocation and renewal of group keying material, namely group rekeying, should be adopted in OSCORE groups. In particular, the key management scheme should preserve backward and forward security in the OSCORE group, if the application requires so (see Section 2.1 of {{I-D.ietf-core-oscore-groupcomm}}).

Group policies should also take into account the time that the key management scheme requires to rekey the group, on one hand, and the expected frequency of group membership changes, i.e. nodes' joining and leaving, on the other hand.

In fact, it may be desirable to not rekey the group upon every single membership change, in case members' joining and leaving are frequent, and at the same time a single group rekeying instance takes a non negligible time to complete.

In such a case, the Group Manager may consider to rekey the group, e.g., after a minimum number of nodes has joined or left the group within a pre-defined time interval, or according to communication patterns with predictable intervals of network inactivity. This would prevent paralizing communications in the group, when a slow rekeying scheme is used and frequently invoked.

This comes at the cost of not continuously preserving backward and forward security, since group rekeying might not occur upon every single group membership change. That is, latest joined nodes would have access to the key material used prior to their join, and thus be able to access past group communications protected with that key material. Similarly, until the group is rekeyed, latest left nodes would preserve access to group communications protected with the retained key material.

### Source Authentication ### {#chap-security-considerations-sec-mode-sauth}

CoAP endpoints using Group OSCORE countersign their outgoing messages, by means of the countersignature algorithm used in the OSCORE group. This ensures source authentication of messages exchanged by CoAP endpoints through CoAP group communication. In fact, it allows to verify that a received message has actually been originated by a specific and identified member of the OSCORE group.

Appendix F of {{I-D.ietf-core-oscore-groupcomm}} discusses a number of cases where a recipient CoAP endpoint may skip the verification of countersignatures, possibly on a per-message basis. However, this is NOT RECOMMENDED. That is, a CoAP endpoint receiving a message secured with Group OSCORE SHOULD always verify the countersignature.

### Counteraction of Attacks ### {#chap-security-considerations-sec-mode-attacks}

Group OSCORE addresses security attacks mentioned in Sections 11.2-11.6 of {{RFC7252}}, with particular reference to their execution over IP multicast. That is: it provides confidentiality and integrity of request/response data through proxies also in multicast settings; it prevents amplification attacks carried out through responses to injected requests over IP multicast; it limits the impact of attacks based on IP spoofing; it prevents cross-protocol attacks; it derives the group key material from, among other things, a Master Secret securely generated by the Group Manager and provided to CoAP endpoints upon their joining of the OSCORE group; countersignatures assure source authentication of exchanged CoAP messages, and hence prevent a group member to be used for subverting security in the whole group.

## Use of CoAP No Response Option ##
The CoAP No Server Response Option {{RFC7967}} could be misused by a malicious client to evoke as much responses from servers to a multicast request as possible, by using the value '0' - Interested in all responses. This can even override the default behaviour of a CoAP server to suppress the response in case there is nothing of interest to respond with. Therefore, this option can be used to perform an amplification attack.
A proposed mitigation is to only allow this Option to relax the standard suppression rules for a resource in case the Option is sent by an authenticated client. If sent by an unauthenticated client, the Option can be used to expand the classes of responses suppressed compared to the default rules but not to reduce the classes of responses suppressed.

## 6LoWPAN ##
In a 6LoWPAN network, a multicast IPv6 packet may be fragmented prior to transmission. A 6LoWPAN Router that forwards a fragmented packet can have a relatively high impact on the occupation of the wireless channel and on the memory load of the local node due to packet buffer occupation. For example, the MPL {{RFC7731}} protocol requires an MPL Forwarder to store the packet for a longer duration, to allow multiple forwarding transmissions to neighboring Forwarders. If only one of the fragments is not received correctly by an MPL Forwarder, the receiver needs to discard all received fragments and it needs to receive all the packet fragments again on a future occasion.

For these reasons, a fragmented IPv6 multicast packet is a possible attack vector in a Denial of Service (DoS) amplification attack. See Section 11.3 of {{RFC7252}} for more details on amplification. To mitigate the risk, applications sending multicast IPv6 requests to 6LoWPAN hosted CoAP servers SHOULD limit the size of the request to avoid 6LoWPAN fragmentation. A 6LoWPAN Router or multicast forwarder SHOULD deprioritize forwarding for multi-fragment 6LoWPAN multicast packets. Also, a 6LoWPAN Border Router SHOULD implement multicast packet filtering to prevent unwanted multicast traffic from entering a 6LoWPAN network from the outside. For example, it could filter out all multicast packet for which there is no known multicast listener on the 6LoWPAN network.

## Wi-Fi ##
In a home automation scenario using Wi-Fi, Wi-Fi security
   should be enabled to prevent rogue nodes from joining.  The Customer
   Premises Equipment (CPE) that enables access to the Internet should
   also have its IP multicast filters set so that it enforces multicast
   scope boundaries to isolate local multicast groups from the rest of
   the Internet (e.g., as per {{RFC6092}}).  In addition, the scope of
   IP multicast transmissions and listeners should be site-local (5) or smaller.  For
   site-local scope, the CPE will be an appropriate multicast scope
   boundary point.

## Monitoring ##

### General Monitoring ###

   CoAP group communication can be used to control a set of
   related devices: for example, simultaneously turn on all the lights in a
   room.  This intrinsically exposes the group to some unique
   monitoring risks that devices not in a group
   are not as vulnerable to.  For example, assume an attacker is able to
   physically see a set of lights turn on in a room.  Then the attacker
   can correlate an observed CoAP group communication message to the observed
   coordinated group action -- even if the CoAP message is (partly) encrypted.  
   This will give the attacker side-channel information
   to plan further attacks (e.g., by determining the members of the
   group some network topology information may be deduced).

### Pervasive Monitoring ###

   A key additional threat consideration for group communication is
   pervasive monitoring {{RFC7258}}.  CoAP group communication solutions that are built on top
   of IP multicast need to pay particular heed to these dangers.  This
   is because IP multicast is easier to intercept (and to secretly
   record) compared to IP unicast.  Also, CoAP traffic is meant for
   the Internet of Things.  This means that CoAP multicast may be used for
   the control and monitoring of critical infrastructure (e.g., lights,
   alarms, etc.) that may be prime targets for attack.

   For example, an attacker may attempt to record all the CoAP traffic
   going over a smart grid (i.e., networked electrical utility)
   and try to determine critical nodes for further attacks.  For
   example, the source node (controller) sends out CoAP group
   communication messages which easily identifies it as a controller.  
   CoAP multicast traffic is inherently more
   vulnerable (compared to unicast) as the same packet may be
   replicated over many links, leading to a higher probability of
   packet capture by a pervasive monitoring system.

   One mitigation is to restrict the
   scope of IP multicast to the minimal scope that fulfills the
   application need.  Thus, for example, site-local IP multicast scope
   is always preferred over global scope IP multicast if this fulfills
   the application needs.

   Even if all CoAP multicast traffic is encrypted/protected,
   an attacker may still attempt to capture this traffic and perform an
   off-line attack in the future.

# IANA Considerations # {#iana}

This document has no actions for IANA.

--- back

# Use Cases # {#appendix-usecases}

To illustrate where and how CoAP-based group communication can be used, this section summarizes the most common use cases. These use cases include both secured and non-secured CoAP usage. Each subsection below covers one particular category of use cases for CoRE. Within each category, a use case may cover multiple application areas such as home IoT, commercial building IoT (sensing and control), industrial IoT/control, or environmental sensing.

## Discovery ##
Discovery of physical devices in a network, or discovery of information entities hosted on network devices, are operations that are usually required in a system during the phases of setup or (re)configuration. When a discovery use case involves devices that need to interact without having been configured previously with a common security context, unsecured CoAP communication is typically used. Discovery may involve a request to a directory server, which provides services to aid clients in the discovery process. One particular type of directory server is the CoRE Resource Directory {{I-D.ietf-core-resource-directory}}; and there may be other types of directories that can be used with CoAP.

### Distributed Device Discovery ### {#sec-uc-dd}
Device discovery is the discovery and identification of networked devices -- optionally only devices of a particular class, type, model, or brand. Group communication is used for distributed device discovery, if a central directory server is not used. Typically in distributed device discovery, a multicast request is sent to a particular address (or address range) and multicast scope of interest, and any devices configured to be discoverable will respond back. For the alternative solution of centralized device discovery a central directory server is accessed through unicast, in which case group communication is not needed. This requires that the address of the central directory is either preconfigured in each device or configured during operation using a protocol.

In CoAP, device discovery can be implemented by CoAP resource discovery requesting (GET) a particular resource that the sought device class, type, model or brand is known to respond to. It can also be implemented using CoAP resource discovery (Section 7 of {{RFC7252}}) and the CoAP query interface defined in Section 4 of {{RFC6690}} to find these particular resources. Also, a multicast GET request to /.well-known/core can be used to discover all CoAP devices.

### Distributed Service Discovery ### {#sec-uc-sd}
Service discovery is the discovery and identification of particular services hosted on network devices. Services can be identified by one or more parameters such as ID, name, protocol, version and/or type. Distributed service discovery involves group communication to reach individual devices hosting a particular service; with a central directory server not being used.

In CoAP, services are represented as resources and service discovery is implemented using resource discovery (Section 7 of {{RFC7252}}) and the CoAP query interface defined in Section 4 of {{RFC6690}}.

### Directory Discovery ### {#sec-uc-dirdiscovery}
This use case is a specific sub-case of Distributed Service Discovery ({{sec-uc-sd}}), in which a device needs to identify the location of a Directory on the network to which it
can e.g. register its own offered services, or to which it can perform queries to identify and locate other devices/services it needs to access on
the network. Section 3.3 of {{RFC7390}} shows an example of discovering a CoRE Resource Directory using CoAP group communication. As defined in {{I-D.ietf-core-resource-directory}}, a resource directory is a web entity that stores information about web resources and implements REST interfaces for registration and lookup of those resources. For example, a device can register itself to a resource directory to let it be found by other devices and/or applications.

## Operational Phase ##
Operational phase use cases describe those operations that occur most frequently in a networked system, during its operational lifetime and regular operation. Regular usage is when the applications on networked devices perform the tasks they were designed for and exchange of application-related data using group communication occurs. Processes like system reconfiguration, group changes, system/device setup, extra group security changes, etc. are not part of regular operation.

### Actuator Group Control ###
Group communication can be beneficial to control actuators that need to act in synchrony, as a group, with strict timing (latency) requirements. Examples are office lighting, stage lighting, street lighting, or audio alert/Public Address systems. Sections 3.4 and 3.5 of {{RFC7390}} show examples of lighting control of a group of 6LoWPAN-connected lights.

### Device Group Status Request ###
To properly monitor the status of systems, there may be a need for ad-hoc, unplanned status updates. Group communication can be used to quickly send out a request to a (potentially large) number of devices for specific information. Each device then responds back with the requested data. Those devices that did not respond to the request can optionally be polled again via reliable unicast communication to complete the dataset. The device group may be defined e.g. as "all temperature sensors on floor 3", or "all lights in wing B". For example, it could be a status request for device temperature, most recent sensor event detected, firmware version, network load, and/or battery level.

### Network-wide Query ###
In some cases a whole network or subnet of multiple IP devices needs to be queried for status or other information. This is similar to the previous use case except that the device group is not defined in terms of its function/type but in terms of its network location. Technically this is also similar to distributed service discovery ({{sec-uc-sd}}) where a query is processed by all devices on a network - except that the query is not about services offered by the device, but rather specific operational data is requested.

### Network-wide / Group Notification ###
In some cases a whole network, or subnet of multiple IP devices, or a specific target group needs to be notified of a status change or other information. This is similar to the previous two use cases except that the recipients are not expected to respond with some information. Unreliable notification can be acceptable in some use cases, in which a recipient does not respond with a confirmation of having received the notification. In such a case, the receiving CoAP server does not have to create a CoAP response. If the sender needs confirmation of reception, the CoAP servers can be configured for that resource to respond with a 2.xx success status after processing a notification request successfully.

## Software Update ##
Multicast can be useful to efficiently distribute new software (firmware, image, application, etc.) to a group of multiple devices. In this case, the group is defined in terms of device type: all devices in the target group are known to be capable of installing and running the new software. The software is distributed as a series of smaller blocks that are collected by all devices and stored in memory. All devices in the target group are usually responsible for integrity verification of the received software; which can be done per-block or for the entire software image once all blocks have been received. Due to the inherent unreliability of CoAP multicast, there needs to be a backup mechanism (e.g. implemented using CoAP unicast) by which a device can individually request missing blocks of a whole software image/entity. Prior to multicast software update, the group of recipients can be separately notified that there is new software available and coming, using the above network-wide or group notification.

# Acknowledgments # {#acknowledgements}
{: numbered="no"}

The authors sincerely thank Thomas Fossati and Jim Schaad for their comments and feedback.

The work on this document has been partly supported by VINNOVA and the Celtic-Next project CRITISEC.

--- fluff
